<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtNama</name>
   <tag></tag>
   <elementGuidId>1351a7ec-ac4f-471a-8ef5-ce668df91b27</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'style__TextFieldInput-sc-12bsx5v-9 bXNrGm' and @name = 'fullname']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>style__TextFieldInput-sc-12bsx5v-9 bXNrGm</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>fullname</value>
   </webElementProperties>
</WebElementEntity>
