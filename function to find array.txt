import java.util.*;
public class Duplicate {

public static void main(String[] args) 
{
    // TODO Auto-generated method stub

    int array1[]= {1,2,4,6,9,50,34};
    int array2[]= {1,5,4,50,24,78,34};

    HashSet<Integer> hashValue=new HashSet<>();
    for(int i=0;i<array1.length;i++) {
        hashValue.add(array1[i]);
    }

    for(int j=0;j<array2.length;j++) {
        if(hashValue.contains(array2[j])) {
            System.out.println("the duplicate value is  "+array2[j]);
        }
    }


}