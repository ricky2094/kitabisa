<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnClosePopup</name>
   <tag></tag>
   <elementGuidId>e8c19e8d-4f19-43f8-8b3d-9c65844115e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'style__BannerCloseIcon-sc-106sadj-2 kCModS']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>style__BannerCloseIcon-sc-106sadj-2 kCModS</value>
   </webElementProperties>
</WebElementEntity>
