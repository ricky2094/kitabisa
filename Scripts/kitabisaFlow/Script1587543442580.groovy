import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.URLKitaBisa)

WebUI.maximizeWindow()

WebUI.scrollToElement(findTestObject('campaignFirst'), 0)

WebUI.click(findTestObject('campaignFirst'))

WebUI.verifyElementVisible(findTestObject('btnDonasiSekarang'))

WebUI.click(findTestObject('btnDonasiSekarang'))

WebUI.click(findTestObject('label10000'))

WebUI.scrollToElement(findTestObject('lblTransferBCA'), 0)

WebUI.click(findTestObject('lblTransferBCA'))

WebUI.setText(findTestObject('txtNama'), GlobalVariable.nama)

WebUI.setText(findTestObject('txtNotel'), GlobalVariable.noTel)

WebUI.click(findTestObject('btnLanjutkanPembayaran'))

WebUI.delay(5)

if (WebUI.verifyElementVisible(findTestObject('Object Repository/btnClosePopup'), FailureHandling.OPTIONAL)) {
    WebUI.click(findTestObject('btnClosePopup'))
} else {
}

orderUrl = WebUI.getUrl()

not_run: WebUI.scrollToElement(findTestObject('btnKembalikePenggalangan'), 0)

not_run: WebUI.verifyElementVisible(findTestObject('btnKembalikePenggalangan'))

WebUI.click(findTestObject('btnKembalikePenggalangan'))

WebUI.click(findTestObject('btnBackonCampaignPage'))

URLNow = WebUI.getUrl()

WebUI.verifyMatch(URLNow, orderUrl, false)

