<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lblTransferBCA</name>
   <tag></tag>
   <elementGuidId>123a3fb2-74fe-4f4c-91f4-6f0d522ab595</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Transfer BCA' or . = 'Transfer BCA')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Transfer BCA</value>
   </webElementProperties>
</WebElementEntity>
